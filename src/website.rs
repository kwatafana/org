//! Tool for managing the [kwatafana.org](https://kwatafana.org/) website

use crate::config;
use std::process::Command;

/// Build website landing page
pub fn index() {
    // Build toml -> html
    Command::new(config::HTOML_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg(config::INDEX_HTOML)
        .status()
        .expect("Failed to build website landing page");
}

/// Everything under kwatafana.org/security
pub mod security {
    use super::*;

    pub fn all() {
        index();
        defense_book();
        offense_book();
        dependable_book();
    }

    pub fn index() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Kwatafana | Cyber Security 🔐")
            .arg("-i")
            .arg(config::SECURITY_INDEX_MD)
            .arg("-o")
            .arg(config::SECURITY_INDEX_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build security/index.html");

        crate::utils::last_updated(config::SECURITY_INDEX_MD);
    }

    pub fn defense_book() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("-i")
            .arg(config::CYBER_DEFENSE_BOOK)
            .arg("-o")
            .arg(config::CYBER_DEFENSE_BOOK_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .arg("--toc")
            .status()
            .expect("Failed to build cyber defense book");
    }

    pub fn offense_book() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("-i")
            .arg(config::CYBER_OFFENSE_BOOK)
            .arg("-o")
            .arg(config::CYBER_OFFENSE_BOOK_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .arg("--toc")
            .status()
            .expect("Failed to build cyber offense book");
    }

    pub fn dependable_book() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("-i")
            .arg(config::CYBER_DEPENDABLE_BOOK)
            .arg("-o")
            .arg(config::CYBER_DEPENDABLE_BOOK_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .arg("--toc")
            .status()
            .expect("Failed to build cyber dependable book");
    }
}

/// Everything under kwatafana.org/articles
pub mod articles {
    use super::*;

    pub fn all() {
        index();
        sqlite_transactions();
        code_snippets();
        systems_coding();
        solana_algo();
        guix();
    }

    pub fn index() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Kwatafana | Articles 📰")
            .arg("-i")
            .arg(config::ARTICLES_INDEX_MD)
            .arg("-o")
            .arg(config::ARTICLES_INDEX_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/index.html");

        crate::utils::last_updated(config::ARTICLES_INDEX_MD);
    }

    pub fn sqlite_transactions() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=SQLite Transactions (Rust)")
            .arg("-i")
            .arg(config::SQLITE_TRANSACTIONS_MD)
            .arg("-o")
            .arg(config::SQLITE_TRANSACTIONS_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/sqlite-transactions.html");
    }

    pub fn code_snippets() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Code Snippets")
            .arg("-i")
            .arg(config::CODE_SNIPPETS_MD)
            .arg("-o")
            .arg(config::CODE_SNIPPETS_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/code-snippets.html");

        crate::utils::last_updated(config::CODE_SNIPPETS_MD);
    }

    pub fn systems_coding() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Systems Coding")
            .arg("-i")
            .arg(config::SYSTEMS_CODING_MD)
            .arg("-o")
            .arg(config::SYSTEMS_CODING_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/systems-coding.html");

        crate::utils::last_updated(config::SYSTEMS_CODING_MD);
    }

    pub fn solana_algo() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Solana Algo-Rythm")
            .arg("-i")
            .arg(config::SOLANA_ALGO_MD)
            .arg("-o")
            .arg(config::SOLANA_ALGO_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .arg("--toc")
            .status()
            .expect("Failed to build articles/systems-coding.html");

        crate::utils::last_updated(config::SOLANA_ALGO_MD);
    }

    pub fn guix() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("-i")
            .arg(config::GUIX_MD)
            .arg("-o")
            .arg(config::GUIX_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .arg("--toc")
            .status()
            .expect("Failed to build articles/guix.html");

        crate::utils::last_updated(config::GUIX_MD);
    }
}

/// Everything under kwatafana.org/essays
pub mod essays {
    use super::*;

    pub fn all() {
        index();
        tunnel_vision();
    }

    pub fn index() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=Kwatafana | Essays 📝")
            .arg("-i")
            .arg(config::ESSAYS_INDEX_MD)
            .arg("-o")
            .arg(config::ESSAYS_INDEX_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/index.html");

        crate::utils::last_updated(config::ESSAYS_INDEX_MD);
    }

    pub fn tunnel_vision() {
        Command::new(config::PANDOC_EXE)
            .current_dir(config::WEBSITE_SRC_DIR)
            .arg("-s")
            .arg("--metadata")
            .arg("title=ISPs and Tunnel Vision")
            .arg("-i")
            .arg(config::TUNNEL_VISION_MD)
            .arg("-o")
            .arg(config::TUNNEL_VISION_HTML)
            .arg("--self-contained")
            .arg(&format!("--css={}", config::MAIN_CSS))
            .status()
            .expect("Failed to build articles/tunnel-vision.html");

        crate::utils::last_updated(config::TUNNEL_VISION_MD);
    }
}
