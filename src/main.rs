mod command;
mod config;
mod git;
mod server;
mod utils;
mod website;

fn main() {
    command::Command::exe();
}
