use chrono::prelude::*;
use std::fs;

/// Removes last line from a file and appends current date to end of
/// file
pub fn last_updated(file: &str) {
    let contents = fs::read_to_string(file).unwrap();
    let date = format!(
        r#"<hr>
<a href='../index.html'>⧉ kwatafana</a> | 
<em>
  <b>email: kwatafana (at) protonmail (dot) com</b>
</em>
<br>
<div style='text-align: right'>
  <small><em>updated: {}</small>
</div>"#,
        Utc::now()
    );

    if let Some(i) = contents.rfind("<hr>") {
        if let Some(s) = contents.get(0..i) {
            let out = format!("{}{}", s, date);
            fs::write(file, out.as_bytes()).unwrap();
        }
    } else {
        let out = format!("{}{}", contents, date);
        fs::write(file, out.as_bytes()).unwrap();
    }
}
