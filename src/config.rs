pub const PANDOC_EXE: &str = "/usr/bin/pandoc";
pub const WEBSITE_SRC_DIR: &str = "/home/kaindume/kwatafana/writings";
pub const WEBSITE_PUB_DIR: &str = "/home/kaindume/kwatafana/www";
pub const HTOML_EXE: &str = "/home/kaindume/kwatafana/software/htoml/target/release/htoml";

/////////// Web Pages /////////////////
pub const INDEX_HTOML: &str = "/home/kaindume/kwatafana/writings/index.toml";
pub const MAIN_CSS: &str = "/home/kaindume/kwatafana/writings/style.css";
///// Security
pub const SECURITY_INDEX_MD: &str = "/home/kaindume/kwatafana/writings/security/index.md";
pub const SECURITY_INDEX_HTML: &str = "/home/kaindume/kwatafana/www/security/index.html";
pub const CYBER_DEFENSE_BOOK: &str = "/home/kaindume/kwatafana/writings/security/secure.md";
pub const CYBER_DEFENSE_BOOK_HTML: &str = "/home/kaindume/kwatafana/www/security/secure.html";
pub const CYBER_OFFENSE_BOOK: &str = "/home/kaindume/kwatafana/writings/security/vulnerable.md";
pub const CYBER_OFFENSE_BOOK_HTML: &str = "/home/kaindume/kwatafana/www/security/vulnerable.html";

pub const CYBER_DEPENDABLE_BOOK: &str = "/home/kaindume/kwatafana/writings/security/dependable.md";
pub const CYBER_DEPENDABLE_BOOK_HTML: &str =
    "/home/kaindume/kwatafana/www/security/dependable.html";

///// Articles
pub const ARTICLES_INDEX_MD: &str = "/home/kaindume/kwatafana/writings/articles/index.md";
pub const ARTICLES_INDEX_HTML: &str = "/home/kaindume/kwatafana/www/articles/index.html";
pub const SQLITE_TRANSACTIONS_MD: &str =
    "/home/kaindume/kwatafana/writings/articles/sqlite-transactions.md";
pub const SQLITE_TRANSACTIONS_HTML: &str =
    "/home/kaindume/kwatafana/www/articles/sqlite-transactions.html";
pub const CODE_SNIPPETS_MD: &str = "/home/kaindume/kwatafana/writings/articles/code-snippets.md";
pub const CODE_SNIPPETS_HTML: &str = "/home/kaindume/kwatafana/www/articles/code-snippets.html";
pub const SYSTEMS_CODING_MD: &str = "/home/kaindume/kwatafana/writings/articles/systems-coding.md";
pub const SYSTEMS_CODING_HTML: &str = "/home/kaindume/kwatafana/www/articles/systems-coding.html";
pub const SOLANA_ALGO_MD: &str = "/home/kaindume/kwatafana/writings/articles/solana-algo-rythm.md";
pub const SOLANA_ALGO_HTML: &str = "/home/kaindume/kwatafana/www/articles/solana-algo-rythm.html";
pub const GUIX_MD: &str = "/home/kaindume/kwatafana/writings/articles/guix.md";
pub const GUIX_HTML: &str = "/home/kaindume/kwatafana/www/articles/guix.html";

///// Essays
pub const ESSAYS_INDEX_MD: &str = "/home/kaindume/kwatafana/writings/essays/index.md";
pub const ESSAYS_INDEX_HTML: &str = "/home/kaindume/kwatafana/www/essays/index.html";
pub const TUNNEL_VISION_MD: &str = "/home/kaindume/kwatafana/writings/essays/tunnel-vision.md";
pub const TUNNEL_VISION_HTML: &str = "/home/kaindume/kwatafana/www/essays/tunnel-vision.html";

/////////// Git /////////////////
pub const STAGIT_EXE: &str = "/usr/local/bin/stagit";
pub const STAGIT_INDEX_EXE: &str = "/usr/local/bin/stagit-index";
pub const INDEX_PUB: &str = "/home/kaindume/kwatafana/www/git/index.html";
pub const REPO_PUB: &str = "/home/kaindume/kwatafana/www/git";
pub const HTOML_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/htoml";
pub const HTOML_REPO: &str = "/home/kaindume/kwatafana/software/htoml";
pub const HTOML_README_MD: &str = "/home/kaindume/kwatafana/software/htoml/README.md";
pub const HTOML_PRETTY_HTML: &str =
    "/home/kaindume/kwatafana/www/git/htoml/file/README.md.pretty.html";
pub const H4_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/h4";
pub const H4_REPO: &str = "/home/kaindume/kwatafana/security/h4";
pub const H4_README_MD: &str = "/home/kaindume/kwatafana/security/h4/README.md";
pub const H4_PRETTY_HTML: &str = "/home/kaindume/kwatafana/www/git/h4/file/README.md.pretty.html";
pub const CYRTOPHORA_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/cyrtophora";
pub const CYRTOPHORA_REPO: &str = "/home/kaindume/kwatafana/security/cyrtophora";
pub const CYRTOPHORA_README_MD: &str = "/home/kaindume/kwatafana/security/cyrtophora/README.md";
pub const CYRTOPHORA_PRETTY_HTML: &str =
    "/home/kaindume/kwatafana/www/git/cyrtophora/file/README.md.pretty.html";
pub const DRTX_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/drtx";
pub const DRTX_REPO: &str = "/home/kaindume/kwatafana/software/drtx";
pub const DRTX_README_MD: &str = "/home/kaindume/kwatafana/software/drtx/README.md";
pub const DRTX_PRETTY_HTML: &str =
    "/home/kaindume/kwatafana/www/git/drtx/file/README.md.pretty.html";
pub const FUNSCHEDULER_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/funscheduler";
pub const FUNSCHEDULER_REPO: &str = "/home/kaindume/kwatafana/software/etango/funscheduler";
pub const FUNSCHEDULER_README_MD: &str =
    "/home/kaindume/kwatafana/software/etango/funscheduler/README.md";
pub const FUNSCHEDULER_PRETTY_HTML: &str =
    "/home/kaindume/kwatafana/www/git/funscheduler/file/README.md.pretty.html";
pub const IETF_REPO_PUB: &str = "/home/kaindume/kwatafana/www/git/ietf";
pub const IETF_REPO: &str = "/home/kaindume/kwatafana/p2p/ietf";
pub const IETF_README_MD: &str = "/home/kaindume/kwatafana/p2p/ietf/README.md";
pub const IETF_PRETTY_HTML: &str =
    "/home/kaindume/kwatafana/www/git/ietf/file/README.md.pretty.html";
