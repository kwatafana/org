use crate::config;
use std::process::Command;

pub fn start() {
    Command::new("python3")
        .current_dir(config::WEBSITE_PUB_DIR)
        .arg("-m")
        .arg("http.server")
        .status()
        .expect("Failed to build website landing page");
}
