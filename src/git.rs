use crate::config;
use std::fs;
use std::process::{Command, Stdio};
use std::str;

pub fn all() {
    index();
    htoml();
    h4();
    cyrtophora();
    drtx();
    funscheduler();
    ietf();
}

pub fn index() {
    let mut i = String::new();
    let s = Command::new(config::STAGIT_INDEX_EXE)
        .current_dir(config::REPO_PUB)
        .arg(config::HTOML_REPO)
        .arg(config::H4_REPO)
        .arg(config::CYRTOPHORA_REPO)
        .arg(config::DRTX_REPO)
        .arg(config::FUNSCHEDULER_REPO)
        .arg(config::IETF_REPO)
        .stdout(Stdio::piped())
        .output()
        .expect("Failed to build git index");

    i.push_str(match str::from_utf8(&s.stdout) {
        Ok(v) => v,
        Err(_) => panic!("got non UTF-8 data from git"),
    });
    fs::write(config::INDEX_PUB, i.as_bytes()).unwrap();
}

pub fn htoml() {
    // Generate stagit repo
    Command::new(config::STAGIT_EXE)
        .current_dir(config::HTOML_REPO_PUB)
        .arg(config::HTOML_REPO)
        .status()
        .expect("Failed to build HTOML");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=htoml")
        .arg("-i")
        .arg(config::HTOML_README_MD)
        .arg("-o")
        .arg(config::HTOML_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to build HTOML");
}

pub fn h4() {
    Command::new(config::STAGIT_EXE)
        .current_dir(config::H4_REPO_PUB)
        .arg(config::H4_REPO)
        .status()
        .expect("Failed to build H4");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=h4")
        .arg("-i")
        .arg(config::H4_README_MD)
        .arg("-o")
        .arg(config::H4_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to H4");
}

pub fn cyrtophora() {
    Command::new(config::STAGIT_EXE)
        .current_dir(config::CYRTOPHORA_REPO_PUB)
        .arg(config::CYRTOPHORA_REPO)
        .status()
        .expect("Failed to build CYRTOPHORA");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=cyrtophora")
        .arg("-i")
        .arg(config::CYRTOPHORA_README_MD)
        .arg("-o")
        .arg(config::CYRTOPHORA_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to build CYRTOPHORA");
}

pub fn drtx() {
    Command::new(config::STAGIT_EXE)
        .current_dir(config::DRTX_REPO_PUB)
        .arg(config::DRTX_REPO)
        .status()
        .expect("Failed to build DRTX");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=drtx")
        .arg("-i")
        .arg(config::DRTX_README_MD)
        .arg("-o")
        .arg(config::DRTX_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to build DRTX");
}

pub fn funscheduler() {
    Command::new(config::STAGIT_EXE)
        .current_dir(config::FUNSCHEDULER_REPO_PUB)
        .arg(config::FUNSCHEDULER_REPO)
        .status()
        .expect("Failed to build FUNSCHEDULER");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=funscheduler")
        .arg("-i")
        .arg(config::FUNSCHEDULER_README_MD)
        .arg("-o")
        .arg(config::FUNSCHEDULER_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to build FUNSCHEDULER");
}

pub fn ietf() {
    Command::new(config::STAGIT_EXE)
        .current_dir(config::IETF_REPO_PUB)
        .arg(config::IETF_REPO)
        .status()
        .expect("Failed to build IETF");

    // Generate pretty README
    Command::new(config::PANDOC_EXE)
        .current_dir(config::WEBSITE_SRC_DIR)
        .arg("-s")
        .arg("--metadata")
        .arg("title=ietf")
        .arg("-i")
        .arg(config::IETF_README_MD)
        .arg("-o")
        .arg(config::IETF_PRETTY_HTML)
        .arg("--self-contained")
        .arg(&format!("--css={}", config::MAIN_CSS))
        .status()
        .expect("Failed to build IETF");
}
