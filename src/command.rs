use crate::server;
use crate::website;
use std::env;
pub struct Command;

impl Command {
    pub fn exe() {
        let sub_command = env::args().nth(1);
        if let Some(sub_command) = sub_command {
            match sub_command.as_str() {
                "website" => WebsiteCommand::exe(),
                "server" => ServerCommand::exe(),
                "help" => {
                    println!(
                        r#"### Web Server

- Startup the web server: `org server start`

### Website Building

#### Main
- landing page: `org website index`
- all website pages: `org website all`

#### Security
- pages index: `org website security index`
- all pages: `org website security all`
- secure-bits page: `org website security secure-bits`
- vulnerable-bits page: `org website security vulnerable-bits`
- dependable-bits page: `org website security dependable-bits`

#### Essays
- essays pages index: `org website essays index`
- all pages: `org website essays all`
- tunnel-vision page: `org website essays tunnel-vision`

#### Articles
- articles pages index: `org website articles index`
- all pages: `org website articles all`
- sqlite-transactions page: `org website articles sqlite-transactions`
- code-snippets page: `org website articles code-snippets`
- systems-coding page: `org website articles systems-coding`
- solana-algo page: `org website articles solana-algo`
- guix page: `org website articles guix`"#
                    )
                }
                _ => panic!("Invalid sub command"),
            }
        } else {
            panic!("Invalid sub command");
        }
    }
}

struct WebsiteCommand;

impl WebsiteCommand {
    fn exe() {
        let arg = env::args().nth(2);
        if let Some(arg) = arg {
            match arg.as_str() {
                "index" => {
                    println!("---[website]: Building website index page");
                    website::index()
                }
                "security" => WebsiteCommand::security(),
                "articles" => WebsiteCommand::articles(),
                "essays" => WebsiteCommand::essays(),
                _ => panic!("Invalid argument"),
            }
        } else {
            panic!("Invalid argument");
        }
    }

    fn security() {
        let flag = env::args().nth(3);
        if let Some(flag) = flag {
            match flag.as_str() {
                "all" => {
                    println!("---[website]: Building all security webpages");
                    website::security::all();
                }
                "index" => {
                    println!("---[website]: Building website security page index");
                    website::security::index()
                }
                "secure-bits" => {
                    println!("---[website]: Building  ⧉ Secure Bits webpage");
                    website::security::defense_book()
                }
                "vulnerable-bits" => {
                    println!("---[website]: Building  ⧉ Vulnerable Bits webpage");
                    website::security::offense_book()
                }
                "dependable-bits" => {
                    println!("---[website]: Building 🔗 Dependable Bits webpage");
                    website::security::dependable_book()
                }
                _ => panic!("Invalid flag"),
            }
        } else {
            panic!("Invalid flag")
        }
    }

    fn articles() {
        let flag = env::args().nth(3);
        if let Some(flag) = flag {
            match flag.as_str() {
                "all" => {
                    println!("---[website]: Building all articles webpages");
                    website::articles::all();
                }
                "index" => {
                    println!("---[website]: Building website articles page index");
                    website::articles::index()
                }
                "sqlite-transactions" => {
                    println!("---[website]: Building website sqlite-transactions articles page");
                    website::articles::sqlite_transactions()
                }
                "code-snippets" => {
                    println!("---[website]: Building website code-snippets articles page");
                    website::articles::code_snippets()
                }
                "systems-coding" => {
                    println!("---[website]: Building website systems-coding articles page");
                    website::articles::systems_coding()
                }
                "solana-algo" => {
                    println!("---[website]: Building website solana-algo articles page");
                    website::articles::solana_algo()
                }
                "guix" => {
                    println!("---[website]: Building website guix articles page");
                    website::articles::guix()
                }
                _ => {
                    panic!("Invalid flag")
                }
            }
        } else {
            panic!("Invalid flag")
        }
    }

    fn essays() {
        let flag = env::args().nth(3);
        if let Some(flag) = flag {
            match flag.as_str() {
                "all" => {
                    println!("---[website]: Building all essay webpages");
                    website::essays::all();
                }
                "index" => {
                    println!("---[website]: Building website essays page index");
                    website::essays::index()
                }
                "tunnel-vision" => {
                    println!("---[website]: Building website tunnel-vision essay page");
                    website::essays::tunnel_vision()
                }
                _ => panic!("Invalid flag"),
            }
        } else {
            panic!("Invalid flag")
        }
    }
}

struct ServerCommand;

impl ServerCommand {
    fn exe() {
        let arg = env::args().nth(2);
        if let Some(arg) = arg {
            match arg.as_str() {
                "start" => {
                    println!("---[server]: Starting the kwatafana web server");
                    server::start()
                }
                _ => panic!("Invalid argument"),
            }
        } else {
            panic!("Invalid argument");
        }
    }
}
