## Commands

### Web Server

- Startup the web server: `org server start`

### Website Building

#### Main
- landing page: `org website index`
- all website pages: `org website all`

#### Security
- pages index: `org website security index`
- all pages: `org website security all`
- secure-bits page: `org website security secure-bits`
- vulnerable-bits page: `org website security vulnerable-bits`
- dependable-bits page: `org website security dependable-bits`

#### Essays
- essays pages index: `org website essays index`
- all pages: `org website essays all`
- tunnel-vision page: `org website essays tunnel-vision`

#### Articles
- articles pages index: `org website articles index`
- all pages: `org website articles all`
- sqlite-transactions page: `org website articles sqlite-transactions`
- code-snippets page: `org website articles code-snippets`
- systems-coding page: `org website articles systems-coding`
- solana-algo page: `org website articles solana-algo`
- guix page: `org website articles guix`
